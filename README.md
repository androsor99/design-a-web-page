# Home task "Design a Web Page"
## Description
Discuss with a tutor choosing one of the following variants of a page.
* Catalogue:
    - Store catalogue,
    - Library catalogue,
    - MOOCs Platform courses catalogue,
    - ...
* Form:
    - Social network registration form,
    - Internet Store request form,
    - Taxes Declaration form,
    - ...
* Report:
    - IT: servers, virtual machines and services availability report,
    - Port workload schedule monthly report,
    - Group's semester results report,
    - ...

Then follow steps described below.

## Steps
* Describe, what information must be shown on a page.
* DDesign a Web Page using HTML and CSS (+JavaScript if necessary).
* Show or send a mentor the designed page.
## Solution
### Social network registration form
* Page title.
* The form must contain the following fields:
    - full name;
    - E-mail;
    - phone number;
    - position;
    - password;
    - gender;
    - data validity checkbox;
* Submit and reset buttons.